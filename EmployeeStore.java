import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;


public class EmployeeStore implements Serializable
{
	private ArrayList<Person> employee;
	private static Scanner kb= new Scanner(System.in);
	IdGenerator empGen;
	
	
	public EmployeeStore()
	{
		this.employee= new ArrayList<Person>();
		
	}
	public EmployeeStore(int lastGenId)
	{
		this.employee= new ArrayList<Person>();
		this.empGen=new IdGenerator(lastGenId);
	}

	public ArrayList<Person> getEmployee() {
		return employee;
	}

	public void setEmployee(ArrayList<Person> employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "Employee [employee=" + employee + "]";
	}
	
	public void addPerson()
	{
		System.out.println("Please Enter an employee name");
		String name = kb.next();
		System.out.println("Please Enter employee email");
		String email = kb.next();
		System.out.println("Please Enter employee tel");
		String tel = kb.next();
		
		
		Person p1= new Person(name,email,tel);
		this.employee.add(p1);
		
		
	}
	
	public void addPersonCheckExist(Person p1)
	{
		//passing person to check if email address exist.
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getPemail().equalsIgnoreCase(p1.getPemail()))
			{
				System.out.println("Employee already exist.");
				found=true;
			}
		}
		if(found==false)
		{
			//adding unique id after we know that person is not duplicated
			(this.empGen.generateUniqueEmp());
			
			this.employee.add(p1);
		}
		
	}
	
	public Person getAnyEmployee(String id)
	{
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{
			return p;		
			}				
		}
			return null;
	}
	
	public void printEmployee()
	{
		for(Person p: employee)
		{
			p.printPerson();	
		}
	}
	
	//this will print first found employee by name
	public void printAnyEmployee(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{p.printPerson();
			found=true;
			break;
			}

				
		}
		if(found==false)
		{
			System.out.println("Sorry, Name doesnt exist!");
		}
	}
	
	//this will edit first found employee by name
	public void editEmployeeByName(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{System.out.println("Please Enter new employee name");
			String newName = kb.next();
			System.out.println("Please Enter new employee email");
			String newEmail = kb.next();
			System.out.println("Please Enter new employee tel");
			String newTel = kb.next();
			
			p.setpName(newName);
			p.setPemail(newEmail);
			p.setPtel(newTel);
			found=true;
			break;}

				
		}
		if(found==false)
		{
			System.out.println("Sorry, Name doesnt exist!");
		}
	}
	
	//delete first found employee by name
	public void deleteEmployeeById(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{employee.remove(p);
			found=true;
			break;
			}

				
		}
		if(found==false)
		{
			System.out.println("Sorry, Couldnt find any one to remove!");
		}
	}
}

