import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TeamTest {
	private Team  team;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPrintTeam() {
		IdGenerator g1= new IdGenerator(0);
		Person p1=new Person("a",g1.generateUniqueEmp(),"gmail", "12345678");
		String result = p1.toString();
		assertEquals(("\nName=\t\t" + "a" +
				"\nId=\t\t" + "EMP1" 
				+ "\nEmail=\t\t" + "gmail"
				+ "\nTel=\t\t" + "12345678" ),result);
		//fail("Not yet implemented");
	}

}
