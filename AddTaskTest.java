import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class AddTaskTest {
	public TaskStore taskStore;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddTask() {
		taskStore = new TaskStore();
		IdGenerator g1= new IdGenerator(0);
		IdGenerator g2= new IdGenerator(0);
		
		Person p1=new Person("a",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p2=new Person("b",g1.generateUniqueEmp(),"gmail", "12345678");
		
		Team t1=new Team(p1);
		t1.addTeammate(p2);
		
		Task tsk1= new Task("Begin work",g2.generateUniqueTsk(),t1,new Date(),new Date(12,11,2013),new Date(12,11,2013),Task.Status.ONGOING,true);
		//fail("Not yet implemented");
		taskStore.addTask(tsk1);
		taskStore.addTask(tsk1);
	}

}
