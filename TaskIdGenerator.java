
public class TaskIdGenerator extends IdGenerator {

	public TaskIdGenerator() {
		
	}

	public TaskIdGenerator(int last) {
		super(last);
		
	}
	
	
	public int generateUniqueTask()
	{
		return this.getLastGenerated()+1;	
	}

}
