import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;


public class MainApp {

	public static void main(String[] args) 
	{
		MainApp theApp= new MainApp();
		//theApp.testingsave();
		theApp.saveFile();

	}
	
	public void start()
	{
		IdGenerator g1= new IdGenerator(0);
		IdGenerator g2= new IdGenerator(0);
		
		Person p1=new Person("a",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p2=new Person("b",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p3=new Person("c",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p4=new Person("d",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p5=new Person("e",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p6=new Person("f",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p7=new Person("g",g1.generateUniqueEmp(),"gmail", "12345678");
		
//		String fileName="data.bin";
//		try{
//			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
//			os.writeObject(p1);
//			os.close();
//			
//		}catch(FileNotFoundException e)
//		{
//			e.printStackTrace();
//		}
//		catch (IOException e){
//			e.printStackTrace();
//		}
//		
//		System.out.println("Done");
		
		
		
		Team t1=new Team(p1);
		t1.addTeammate(p2);
		t1.addTeammate(p3);
		t1.addTeammate(p4);
		
		Team t2=new Team(p2);
		t2.addTeammate(p2);
		t2.addTeammate(p3);
		t2.addTeammate(p4);
		//public Task(String tName, String tID, Team taskTeam, Calendar addDate,
			//	Calendar dueDate, Calendar completeDate, Byte status,
			//	boolean overdue)
		
		Date addedOn= new Date();
		Date DueOn= new Date(12,12,2013);
		Date CompletedOn= new Date();
		
		TaskStore tsStore= new TaskStore();
		Task tsk1= new Task("Begin work",g2.generateUniqueTsk(),t1,new Date(),new Date(12,11,2013),CompletedOn,Task.Status.ONGOING,true);
		tsStore.addTask(tsk1);
		Task tsk2= new Task("Begin work2",g2.generateUniqueTsk(),t2,addedOn,new Date(12,1,2013),CompletedOn,Task.Status.CANCELLED,true);
		tsStore.addTask(tsk2);
		Task tsk3= new Task("Begin work3",g2.generateUniqueTsk(),t2,addedOn,new Date(12,5,2013),CompletedOn,Task.Status.COMPLETED,true);
		tsStore.addTask(tsk3);
		Task tsk4= new Task("Begin work4",g2.generateUniqueTsk(),t1,addedOn,new Date(12,11,2013),CompletedOn,Task.Status.PAUSED,true);
		tsStore.addTask(tsk4);
		Task tsk5= new Task("Begin work5",g2.generateUniqueTsk(),t1,addedOn,new Date(1,11,2013),CompletedOn,Task.Status.ONGOING,true);
		tsStore.addTask(tsk5);
		Task tsk6= new Task("Begin work6",g2.generateUniqueTsk(),t2,addedOn,new Date(2,11,2013),CompletedOn,Task.Status.CANCELLED,true);
		tsStore.addTask(tsk6);
		Task tsk7= new Task("Begin work7",g2.generateUniqueTsk(),t1,addedOn,new Date(1,11,2013),CompletedOn,Task.Status.COMPLETED,true);
		tsStore.addTask(tsk7);
		Task tsk8= new Task("Begin work8",g2.generateUniqueTsk(),t1,addedOn,new Date(4,11,2013),CompletedOn,Task.Status.PAUSED,true);
		tsStore.addTask(tsk8);
		
		//SerializationUtility.save("TaskStore.bin", tsStore);
		SerializationUtility.save("Store.bin", tsStore);
		TaskStore loadedStore= (TaskStore)SerializationUtility.load("Store.bin");
		loadedStore.printStore();
		
//		ArrayList<Task> sortedList= tsStore.getSortedByStatus();
//		printArrayList(sortedList);
//
//		
//		tsStore.printStore();
		
		//TaskStore tsStore2= tsStore.copyStore();
//		ArrayList<Task> tsStore2= tsStore.copyStore();
//		
//		System.out.println("Copied:-------------------------------------");
//		printArrayList(tsStore2);
//		
//		System.out.println("To check if nth is changed!-----------------------------------");
//		tsStore.printStore();
		
//		tsStore.printTaskWithSameLeader("EMP1");
//		
//		Date one=new Date(1,1,2013);
//		Date two=new Date(31,7,2013);
//		
//		ArrayList<Task> checkdate= tsStore.getTaskBetweenTwoDates(one, two);
//		printArrayList(checkdate);
		
	
		//p1.printPerson();
		//ArrayList<Task> leaderOverdue= tsStore.getTaskOverdueForLeader(p1.getpId(),8);
	//	printArrayList(leaderOverdue);
		//p1.printPerson();
		
		
	}
	
	public void start2()
	{
		
		Employee eStore= new Employee();
		
		eStore.printEmployee();
		eStore.printAnyEmployee("a");
		eStore.editEmployeeByName("a");
		eStore.printEmployee();
		eStore.deleteEmployeeById("EMP1");
		eStore.printEmployee();
		
		
		
	}
	
	private void sort(ArrayList<Task> list, Comparator<Task> comp)
	{
		Collections.sort(list,comp);
		
	}
	
	private void printArrayList(ArrayList<Task> tList)
	{
		for(Task t: tList)
			{
				t.printTask();
			}
		
	}
	
	public void testingDate()
	{
		//http://stackoverflow.com/questions/3299972/difference-in-days-between-two-dates-in-java
		Date today= new Date(9,11,2013);
		Date due= new Date(12,11,2013);
		//Date dateToCOmpare= new Date(12,11,2013)
		//int days = Days.daysBetween(today, dateToCOmpare).getDays();
		//int day= today.getEventDate().DAY_OF_MONTH;
		long tt=today.getEventDate().getTimeInMillis();
		long hh=due.getEventDate().getTimeInMillis();
		long diff = hh-tt;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		//System.out.println(diffDays);
		//int days = Days.daysBetween(new DateTime(past), new DateTime(today.getEventDate())).getDays();
		
	}
	
//	  public int daysBetween(Date d1, Date d2)
//	  {
//          return (int) ((d2.getEventDate().getTime() - d1.getEventDate().getTime())) / (1000 * 60 * 60 * 24));
//	  }
	
	public void testingsave()
	{
		IdGenerator g1= new IdGenerator(0);

		
		Person p1=new Person("a",g1.generateUniqueEmp(),"gmail", "12345678");
		Person p2;
		SerializationUtility.save("Person.bin", p1);
		p2=(Person)SerializationUtility.load("Person.bin");
		
		p2.printPerson();
	}
	
	public void testEmail()
	{
		MailUtility.send("d00142486@student.dkit.ie", "Testing on java project", "bla bla bla", "text/plain");
	}

	public void saveFile()
	{
		TaskStore tsStore= new TaskStore();
		
		IdGenerator g1= new IdGenerator(0);
		IdGenerator g2= new IdGenerator(0);
		
		Person p1=new Person("Lukas Ponik","lukas@gmail.com", "0871794751");
		Person p2=new Person("Audreen Soh","Audreen@gmail.com", "0877552062");
		Person p3=new Person("Cormac Matthews","cormac@gmail.com", "087342122");
		Person p4=new Person("Darren","darren@gmail.com", "087123456");
		Person p5=new Person("Anna ","anna@gmail.com", "0853214325");
		Person p6=new Person("Philip","philip@gmail.com", "083567890");
		Person p7=new Person("Naill","naill@gmail.com", "0879876543");
		

		Employee elist= new Employee();
		elist.addPerson(p1);
		elist.addPerson(p2);
		elist.addPerson(p3);
		elist.addPerson(p4);
		elist.addPerson(p5);
		elist.addPerson(p6);
		elist.addPerson(p7);
		
		Team t1=new Team(elist.getAnyEmployee("EMP1"));
		t1.addTeammate(elist.getAnyEmployee("EMP2"));
		t1.addTeammate(elist.getAnyEmployee("EMP6"));
		t1.addTeammate(elist.getAnyEmployee("EMP7"));
		
		Team t2=new Team(elist.getAnyEmployee("EMP2"));
		t2.addTeammate(elist.getAnyEmployee("EMP4"));
		t2.addTeammate(elist.getAnyEmployee("EMP5"));
		t2.addTeammate(elist.getAnyEmployee("EMP3"));
		
		
		Task tsk1= new Task("Create MicroChip",g2.generateUniqueTsk(),t1,new Date(5,4,2012),new Date(12,11,2013),Task.Status.ONGOING,true);
		tsStore.addTask(tsk1);
		Task tsk2= new Task("Welcoming the president",g2.generateUniqueTsk(),t2,new Date(1,11,2013),new Date(20,11,2013),new Date(18,11,2013),Task.Status.COMPLETED,false);
		tsStore.addTask(tsk2);
		Task tsk3= new Task("Programming the chip",g2.generateUniqueTsk(),t2,new Date(5,4,2012),new Date(5,10,2013),Task.Status.ONGOING,true);
		tsStore.addTask(tsk3);
		Task tsk4= new Task("Create model of Airplane",g2.generateUniqueTsk(),t1,new Date(30,5,2013),new Date(30,6,2014),Task.Status.PAUSED,false);
		tsStore.addTask(tsk4);
		Task tsk5= new Task("GUI design for Samsung S200",g2.generateUniqueTsk(),t1,new Date(30,8,2013),new Date(31,0,2014),Task.Status.CANCELLED,false);
		tsStore.addTask(tsk5);

		tsStore.printStore();
	
		SerializationUtility.save("Store.bin", tsStore);
		//TaskStore loadedStore= (TaskStore)SerializationUtility.load("Store.bin");
		//loadedStore.printStore();
		SerializationUtility.save("PersonID.bin", g1);
		SerializationUtility.save("TaskID.bin", g2);
		SerializationUtility.save("AllEmployees.bin", elist);
	}
	
	public void startWithMenu()
	{
		IdGenerator PersonIDGen= (IdGenerator)SerializationUtility.load("PersonID.bin");
		IdGenerator TaskIDGen= (IdGenerator)SerializationUtility.load("TaskID.bin");
		
		TaskStore loadedStore= (TaskStore)SerializationUtility.load("Store.bin");
		Employee elist=(Employee)SerializationUtility.load("AllEmployees.bin");
		
		
	}
}
