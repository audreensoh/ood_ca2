import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PrintAnyClassTest {
	public TaskStore taskStore;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPrintStore() {
		
		taskStore = new TaskStore();
		IdGenerator g1= new IdGenerator(0);
		IdGenerator g2= new IdGenerator(0);
		
		Person p1=new Person("a",g1.generateUniqueEmp(),"gmail", "12345678");

		Team t1=new Team(p1);
		
		Task tsk1= new Task("Begin work",g2.generateUniqueTsk(),t1,new Date(),new Date(12,11,2013),new Date(12,11,2013),Task.Status.ONGOING,true);
		//fail("Not yet implemented");
		taskStore.addTask(tsk1);
		//fail("Not yet implemented");
		String result = tsk1.toString();
		
		assertEquals(("\nTask=\t\t" + "Begin work" +
				"\ntID=\t\t" + "1" + 
				"taskTeam=\n"	+"Team Leader="
				+"\nName=\t\t" + "a" +
				"\nId=\t\t" + "EMP1" 
				+ "\nEmail=\t\t" + "gmail"
				+ "\nTel=\t\t" + "12345678"  + 
				"addDate=" + "10 Dec 2013" + 
				"dueDate=" + "12 Dec 2013"+ 
				"completeDate=" + "12 Dec 2013" + 
				"status=" + "ONGOING"
				+ "overdue=" + "true" ),result);
	}

}
