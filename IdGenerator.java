import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;


public class IdGenerator implements Serializable
{
	private HashSet<Integer> alreadyGenerated;
	private int lastGenerated;
	
	public IdGenerator()
	{
		this.alreadyGenerated=new HashSet<Integer>();
		this.lastGenerated=0;
		
	}
	
	public IdGenerator(int last)
	{
		this.alreadyGenerated=new HashSet<Integer>();
		this.lastGenerated=last;
		
	}
	
	
	public HashSet<Integer> getAlreadyGenerated() {
		return alreadyGenerated;
	}

	public void setAlreadyGenerated(HashSet<Integer> alreadyGenerated) {
		this.alreadyGenerated = alreadyGenerated;
	}

	public int getLastGenerated() {
		return lastGenerated;
	}

	public void setLastGenerated(int lastGenerated) {
		this.lastGenerated = lastGenerated;
	}

	public void addAlreadyGenerated(int n)
	{
		this.alreadyGenerated.add(n);
		
	}
	
	//if already contains then add, if not contained then add it 
	public String generateUniqueEmp()
	{
		this.lastGenerated++;
		return "EMP"+ this.lastGenerated;	
	}
	
	public int generateUniqueTsk()
	{
		this.lastGenerated++;
		return this.lastGenerated;
	}
	
}


