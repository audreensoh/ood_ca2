import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;


public class Employee implements Serializable
{
	private ArrayList<Person> employee;
	private static Scanner kb= new Scanner(System.in);
	IdGenerator empGen;
	
	
	public Employee()
	{
		this.employee= new ArrayList<Person>();
		this.empGen=new IdGenerator();
		
	}
	public Employee(int lastGenId)
	{
		this.employee= new ArrayList<Person>();
		this.empGen=new IdGenerator(lastGenId);
	}

	public ArrayList<Person> getEmployee() {
		return employee;
	}

	public void setEmployee(ArrayList<Person> employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "Employee [employee=" + employee + "]";
	}
	
	public void addPerson(Person p)
	{
		this.addPersonCheckExist(p);	
	}
	
	public void addPerson()
	{
		System.out.println("Please Enter an employee name");
		String name = kb.next();
		System.out.println("Please Enter employee email");
		String email = kb.next();
		System.out.println("Please Enter employee tel");
		String tel = kb.next();
		
		
		Person p1= new Person(name,email,tel);
		this.addPersonCheckExist(p1);
		
		
	}
	
	public void addPersonCheckExist(Person p1)
	{
		//passing person to check if email address exist.
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getPemail().equalsIgnoreCase(p1.getPemail()))
			{
				System.out.println("Employee already exist.");
				found=true;
			}
		}
		
		if(found==false)
		{
			//adding unique id after we know that person is not duplicated
			String idGenerated=this.empGen.generateUniqueEmp();
		p1.setpId(idGenerated);
			
			this.employee.add(p1);
			System.out.println("Employee added.");
		}
		
	}
	
	public Person getAnyEmployee(String id)
	{
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{
			return p;		
			}				
		}
			return null;
	}
	
	public void printEmployees()
	{
		for(Person p: employee)
		{
			p.printPerson();	
		}
	}
	
	public void printDetailsOfAnEmplyee()
	{
		System.out.println("Please an ID of employee you wan to print");
		String id = kb.next();
		
		this.printAnyEmployee(id);
	}
	
	//this will print first found employee by name
	public void printAnyEmployee(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{p.printPerson();
			found=true;
			break;
			}

				
		}
		if(found==false)
		{
			System.out.println("Sorry, Name doesnt exist by this id!");
		}
	}
	
	//this will edit first found employee by name
	public void editEmployeeByName(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{System.out.println("Please Enter new employee name");
			String newName = kb.next();
			System.out.println("Please Enter new employee email");
			String newEmail = kb.next();
			System.out.println("Please Enter new employee tel");
			String newTel = kb.next();
			
			p.setpName(newName);
			p.setPemail(newEmail);
			p.setPtel(newTel);
			found=true;
			break;}

				
		}
		if(found==false)
		{
			System.out.println("Sorry, Name doesnt exist!");
		}
	}
	
	//delete first found employee by name
	public void deleteEmployee()
	{
		System.out.println("Please Enter an ID of emplyee you want to delete");
		String id = kb.next();
		this.deleteEmployeeById(id);
	}
	//delete first found employee by name
	public void deleteEmployeeById(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{employee.remove(p);
			System.out.println("Emplyee " +id+ "removed!");
			found=true;
			break;
			}

				
		}
		if(found==false)
		{
			System.out.println("Sorry, Couldnt find any one to remove!");
		}
	}
	public void saveEmployeeStore()
	{
		
		System.out.println("Enter file name to save employee.");
		String file = kb.next();
		file=file+".bin";
		saveEmployeeStore(file);
	}
	
	public void saveEmployeeStore(String filename)
	{
		SerializationUtility.save(filename, this);
	}
	
	public Employee loadEmployeeStore(String filename)
	{
		return (Employee)SerializationUtility.load(filename);
	}
	
	public void editEmployeeById()
	{
		System.out.println("Enter employee id.");
		String id= kb.next();
		this.editEmployeeById(id);

		
	}
	
	public void editEmployeeById(String id)
	{
		boolean found=false;
		for(Person p: employee)
		{
			if(p.getpId().equalsIgnoreCase(id))
			{
				p.editPerson();
				found=true;
				break;
			}
			 
				
		}
		if (found==false)
		{
			System.out.println("Sorry the person by that ID doesnt exists.");

		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
}

