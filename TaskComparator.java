import java.util.Comparator;

public class TaskComparator implements Comparator<Task>
{
	
	
	public TaskComparator()
	{
		
		
	}
	
	public int compare(Task one,Task two)
	{
		
		
			return one.getStatus().compareTo(two.getStatus());
		
		
	}

	
}
