
public class MainAppTaskManager {

	public static void main(String[] args)
	{
		MainAppTaskManager theApp= new MainAppTaskManager();
		//theApp.testingsave();
		theApp.saveFile();
		theApp.startWithMenu();
		//theApp.sendMail();
	}
	
	public void saveFile()
	{
		TaskStore tsStore= new TaskStore();
			
		Person p1=new Person("Lukas Ponik","lukas@gmail.com", "0871794751");
		Person p2=new Person("Audreen Soh","Audreen@gmail.com", "0877552062");
		Person p3=new Person("Cormac Matthews","cormac@gmail.com", "087342122");
		Person p4=new Person("Darren","darren@gmail.com", "087123456");
		Person p5=new Person("Anna ","anna@gmail.com", "0853214325");
		Person p6=new Person("Philip","philip@gmail.com", "083567890");
		Person p7=new Person("Naill","naill@gmail.com", "0879876543");
		

		Employee elist= new Employee();
		elist.addPerson(p1);
		elist.addPerson(p2);
		elist.addPerson(p3);
		elist.addPerson(p4);
		elist.addPerson(p5);
		elist.addPerson(p6);
		elist.addPerson(p7);
		
		Team t1=new Team(elist.getAnyEmployee("EMP1"));
		t1.addTeammate(elist.getAnyEmployee("EMP2"));
		t1.addTeammate(elist.getAnyEmployee("EMP6"));
		t1.addTeammate(elist.getAnyEmployee("EMP7"));
		
		Team t2=new Team(elist.getAnyEmployee("EMP2"));
		t2.addTeammate(elist.getAnyEmployee("EMP4"));
		t2.addTeammate(elist.getAnyEmployee("EMP5"));
		t2.addTeammate(elist.getAnyEmployee("EMP3"));
		
		
		Task tsk1= new Task("Create MicroChip",t1,new Date(5,4,2012),new Date(11,10,2013),Task.Status.ONGOING,true);
		tsStore.addTask(tsk1);
		Task tsk2= new Task("Welcoming the president",t2,new Date(1,11,2013),new Date(20,11,2013),new Date(7,11,2013),Task.Status.COMPLETED,false);
		tsStore.addTask(tsk2);
		Task tsk3= new Task("Programming the chip",t2,new Date(5,4,2012),new Date(5,10,2013),Task.Status.ONGOING,true);
		tsStore.addTask(tsk3);
		Task tsk4= new Task("Create model of Airplane",t1,new Date(30,5,2013),new Date(30,6,2014),Task.Status.PAUSED,false);
		tsStore.addTask(tsk4);
		Task tsk5= new Task("GUI design for Samsung S200",t1,new Date(30,8,2013),new Date(31,0,2014),Task.Status.CANCELLED,false);
		tsStore.addTask(tsk5);

		//tsStore.printStore();
		tsStore.saveTaskStore("t.bin");
		//TaskStore copiedOne = tsStore.copyStore();
		elist.saveEmployeeStore("e.bin");
		//TaskStore loadedStore= (TaskStore)SerializationUtility.load("Store.bin");
		//loadedStore.printStore();
		elist.printEmployees();
		
	}

	public void startWithMenu()
	{
	
		TaskStore tsStore = new TaskStore();
		Employee elist = new Employee();
		elist = (Employee)SerializationUtility.load("e.bin");
		tsStore = (TaskStore)SerializationUtility.load("t.bin");
		boolean done= false;
		while(done==false)
		{	
			MenuFunctions.printMenu(elist, tsStore);
		}
	/*	tsStore.printStore();
		System.out.println("-----------------------------------------------------------------");
		//elist.printEmployees();
		
		Person p1=new Person("Lukass Ponik","lukas@gmsail.com", "0871794751");
		Person p2=new Person("Audreens Soh","Audreen@gmasil.com", "0877552062");
		Person p3=new Person("Cormasc Matthews","cormac@gmsail.com", "087342122");
		Person p4=new Person("Darrsen","darren@gmsail.com", "087123456");
		Person p5=new Person("Annas ","anna@gmaisl.com", "0853214325");
		Person p6=new Person("Philsip","philip@gmasil.com", "083567890");
		Person p7=new Person("Nailsl","naill@gmasil.com", "0879876543");
		

		
		elist.addPerson(p1);
		elist.addPerson(p2);
		elist.addPerson(p3);
		elist.addPerson(p4);
		elist.addPerson(p5);
		elist.addPerson(p6);
		elist.addPerson(p7);
		System.out.println("-----------------------------------------------------------------");
		elist.printEmployees();
		Team t1=new Team(elist.getAnyEmployee("EMP1"));
		t1.addTeammate(elist.getAnyEmployee("EMP2"));
		t1.addTeammate(elist.getAnyEmployee("EMP6"));
		t1.addTeammate(elist.getAnyEmployee("EMP7"));
		
		Team t2=new Team(elist.getAnyEmployee("EMP2"));
		t2.addTeammate(elist.getAnyEmployee("EMP4"));
		t2.addTeammate(elist.getAnyEmployee("EMP5"));
		t2.addTeammate(elist.getAnyEmployee("EMP3"));
		
		
		Task tsk1= new Task("Creaasdte MicroChip",t1,new Date(5,4,2012),new Date(12,11,2013),Task.Status.ONGOING,true);
		tsStore.addTask(tsk1);
		Task tsk2= new Task("Welcasdoming the president",t2,new Date(1,11,2013),new Date(20,11,2013),new Date(7,11,2013),Task.Status.COMPLETED,false);
		tsStore.addTask(tsk2);
		Task tsk3= new Task("Programmasding the chip",t2,new Date(5,4,2012),new Date(5,10,2013),Task.Status.ONGOING,true);
		tsStore.addTask(tsk3);
		Task tsk4= new Task("Create asdmodel of Airplane",t1,new Date(30,5,2013),new Date(30,6,2014),Task.Status.PAUSED,false);
		tsStore.addTask(tsk4);
		Task tsk5= new Task("GUI desiasdgn for Samsung S200",t1,new Date(30,8,2013),new Date(31,0,2014),Task.Status.CANCELLED,false);
		tsStore.addTask(tsk5);
		
		tsStore.printStore();
		*/
			
	}
	
	/*public void sendMail()
	{
		MailUtility.send("d00142486@student.dkit.ie", "Testing on java project", "bla bla bla", "text/plain");
	}*/
}
