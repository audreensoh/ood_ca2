import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;


public class TaskStore implements Cloneable, Serializable
{
	private ArrayList<Task> tStore;
	private static Scanner kb= new Scanner(System.in);
	IdGenerator tskeGen;
	//public static int idS;
	
	public TaskStore()
	{
		this.tStore=new ArrayList<Task>();
		this.tskeGen=new IdGenerator();
	}
	public TaskStore(ArrayList<Task> tStore) 
	{
		//super();
		this.tStore = tStore;
	}

	public ArrayList<Task> gettStore() {
		return tStore;
	}

	public void settStore(ArrayList<Task> tStore) {
		this.tStore = tStore;
	}
	
	public void addTask(Employee empStore)
	{	
		Task toAdd= new Task();
		toAdd.setTask();
		Team myChoosenT = new Team();
		myChoosenT.createTeam(empStore);
		toAdd.setTaskTeam(myChoosenT);
		this.addTask(toAdd);
	}

	public void addTask(Task t1)
	{
		boolean found=false;
		for(Task t: tStore)
		{
			if(t.gettName().equalsIgnoreCase(t1.gettName()))
			{
				System.out.println("Task already exist!");
				found=true;
			}
			
		}
		if(found==false)
		{
			
			t1.settID(this.tskeGen.generateUniqueTsk());//generate id whe you are adding task
			System.out.println("The task id is: " + t1.gettID());
			this.tStore.add(t1);
		}		
	}
	public void deleteTask()
	{
		System.out.println("Please enter id of task to delete:");
		int id = kb.nextInt();
		this.deleteTask(id);
	}
	public void deleteTask(int id)
	{
		boolean found=false;
		for(Task t: tStore)
		{
			if(t.gettID()==id)
			{
				System.out.println("Deleting");
				found=true;
				this.tStore.remove(t);
				break;
			}
			
		}
		if(found==false)
		{
			System.out.println("Didnt find task to delete");
		}		
	}
	public Task getTask(String name)
	{
		for(Task t: tStore)
		{
			if(t.gettName().equalsIgnoreCase(name))
			{
				return t;
			}
		}
			return null;
			
	}
	
	public void printStore()
	{
		for(Task t: tStore)
		{
			t.printTask();
		}
		
	}
	public void printTaskWithSameLeader()
	{
		System.out.println("Please enter id of the leader that you want to display all tasks for");
		String name = kb.next();
		this.printTaskWithSameLeader(name);
	}
	
	public void printTaskWithSameLeader(String id)
	{
		boolean found=false;
		for(Task t:tStore)
		{
			if(t.getTaskTeam().gettLeader().getpId().equalsIgnoreCase(id))
			{
				found=true;
				t.printTask();
			}
			
		}
		if(found==false)
		{
			System.out.println("No task for id ("+ id +")." );
		}
		//System.out.println("All task printed");
		//for testing 
		//getSameStatus(Task.Status.CANCELLED)
	}
	
	public ArrayList<Task> getTaskWithSameLeader(String id)
	{
		ArrayList<Task> getTaskfromALeader = new ArrayList<Task>();
		
		boolean found=false;
		for(Task t:tStore)
		{
			if(t.getTaskTeam().gettLeader().getpId().equalsIgnoreCase(id))
			{
				found=true;
				getTaskfromALeader.add(t);
			}
			
		}
		if(found==false)
		{
			System.out.println("No task for id ("+ id +")." );
			return null;
		}
		
		return getTaskfromALeader;
		//System.out.println("All task printed");
		//for testing 
		//getSameStatus(Task.Status.CANCELLED)
	}
	public void getSameStatus()
	{
		System.out.println("Please choose Status to display tasks with:");
		System.out.println("Enter  1 for CANCELLED");
		System.out.println("Enter  2 for ONGOING");
		System.out.println("Enter  3 for PAUSED");
		System.out.println("Enter  4 for COMPLETED");
		String input = kb.next();
		
		if(input.equalsIgnoreCase("1"))
		{
			this.getSameStatusPrint(Task.Status.CANCELLED);
		}else if(input.equalsIgnoreCase("2"))
		{
			this.getSameStatusPrint(Task.Status.ONGOING);
		}else if(input.equalsIgnoreCase("3"))
		{
			this.getSameStatusPrint(Task.Status.PAUSED);
		}else if(input.equalsIgnoreCase("4"))
		{
			this.getSameStatusPrint(Task.Status.COMPLETED);
		}
		else
		{
			System.out.println("Invalid input.");
		}	
	}
	public void getSameStatusPrint(Task.Status status)
	{
		boolean found=false;
		
		for(Task t:tStore)
		{
			if(t.getStatus() == status)
			{
			t.printTask();
				found=true;
			}
			
		}
		if (found==false)
		{
			System.out.println("No tasks by that status to print.");
		}
		
	}
	
	public ArrayList<Task> getSameStatus(Task.Status status)
	{
		boolean found=false;
		ArrayList<Task> toReturn=new ArrayList<Task>();
		for(Task t:tStore)
		{
			if(t.getStatus() == status)
			{
				toReturn.add(t);
				found=true;
			}
			
		}
		if (found==false)
		{
			return null;
		}
		return toReturn;
	}
	
	public void getSortedByStatusPrint()
	{
		ArrayList<Task> soterByStatusList = this.getSortedByStatus();
		for(Task t : soterByStatusList )
		{
			t.printTask();
		}
			
	}

	public ArrayList<Task> getSortedByStatus()
	{
		TaskComparator tc=new TaskComparator();
		//clone will give you a new object or both ts and this.tStore will point to the same arraylist
		ArrayList<Task> ts=  (ArrayList<Task>) this.tStore.clone(); 
		Collections.sort(ts,tc);
		
		return ts;
	}
	
//	public TaskStore copyStore()
//	{
//		TaskStore ts= new TaskStore();
//		for(Task t: this.tStore)
//		{
//			ts.addTask(t);
//		}
//		
//		return ts;
//	}
	
	public void copyStoreWithSave()
	{
		System.out.println("Please enter destination file name:");
		String filename = kb.next();
		ArrayList<Task> clonedStore = this.copyStore();
		TaskStore clonedOne = new TaskStore();
		clonedOne.settStore(clonedStore);
	
		SerializationUtility.save(filename, clonedOne);
		System.out.println("Store saved in "+ filename);
	}
	
	public ArrayList<Task> copyStore()
	{
		ArrayList<Task> forclone = (ArrayList<Task>) this.tStore.clone();
		
		return forclone;
	}
	
	public void getTaskBetweenTwoDatesPrint() 
	{	
		System.out.println("Setup the first date");
		Date one = new Date();
		System.out.println("Setup the second date");
		Date two = new Date();
		one.setEventDate();
		two.setEventDate();
		this.getTaskBetweenTwoDatesPrint(one, two);
	}
	
	public void getTaskBetweenTwoDatesPrint(Date one, Date two) 
	{		
		boolean found=false;
		if(one.getEventDate().after(two.getEventDate()))
		{
			System.out.println("Sorry, the second date must be after first date.");
		}
		else
		{
			for(Task t: this.tStore)
			{
				if((t.getDueDate().getEventDate().before(one.getEventDate())) || (t.getDueDate().getEventDate().after(two.getEventDate())))
				{
					
				}
				else
				{
					t.printTask();
					found=true;
				}
			}
		}
		
		if(found==false)
		{
			System.out.println("No Task found in that date range");
			
		}
	}
	
	public ArrayList<Task> getTaskBetweenTwoDates(Date one, Date two) 
	{
		ArrayList<Task> taskBetweenRange= new ArrayList<Task>();
		boolean found=false;
		if(one.getEventDate().after(two.getEventDate()))
		{
			System.out.println("Sorry, the second date must be after first date.");
		}
		else
		{
			for(Task t: this.tStore)
			{
				if((t.getDueDate().getEventDate().before(one.getEventDate())) || (t.getDueDate().getEventDate().after(two.getEventDate())))
				{
					
				}
				else
				{
					taskBetweenRange.add(t);
					found=true;
				}
			}
		}
		
		if(found==false)
		{
			System.out.println("No Task found in that date range");
			return null;
		}
		
		return taskBetweenRange;
		
	}
	public void getTaskOverdueForLeader() 
	{
		
		System.out.println("Please enter leader id:");
		String id = kb.next();
		System.out.println("Please enter numbers of days overdue:");
		int days = kb.nextInt();
		this.getTaskOverdueForLeaderPrint(id,days);
		
	}
	
	public void getTaskOverdueForLeaderPrint(String id,int days) 
	{
		
		//put all task of a leader into arraylist, then we check for all his overdue tasks
		ArrayList<Task> taskOfLeaderOverdue= getTaskWithSameLeader(id);
		long todayTimeMilis=0;
		long dueTimeMilis=0;
		long diff=0;
		long diffDays=0;
		Date today= new Date();
		boolean found=false;
		//Iterator<Task> iter=taskOfLeaderOverdue.iterator();
		Task t = new Task(); //for clear readability
			for(int i=0;i!=taskOfLeaderOverdue.size();i++)
			{
				t=taskOfLeaderOverdue.get(i);
				if(t.getDueDate().getEventDate().before(today.getEventDate()))				
				{
						//http://stackoverflow.com/questions/3299972/difference-in-days-between-two-dates-in-java
						todayTimeMilis=today.getEventDate().getTimeInMillis();
						dueTimeMilis=t.getDueDate().getEventDate().getTimeInMillis();
						diff =todayTimeMilis- dueTimeMilis;
						diffDays = diff / (24 * 60 * 60 * 1000);//difference in days
						if(diffDays>=days)
						{
							t.printTask();
						//System.out.println(diffDays);
							found=true;
						}				
				}
			}
		if(found==false)
		{
			System.out.println("No Task found in that date range");
		}

				
	}
	
	public ArrayList<Task> getTaskOverdueForLeader(String id,int days) 
	{
		
		//put all task of a leader into arraylist, then we check for all his overdue tasks
		ArrayList<Task> taskOfLeaderOverdue= getTaskWithSameLeader(id);
		long todayTimeMilis=0;
		long dueTimeMilis=0;
		long diff=0;
		long diffDays=0;
		Date today= new Date();
		boolean found=false;
		//Iterator<Task> iter=taskOfLeaderOverdue.iterator();
		Task t = new Task(); //for clear readability
			for(int i=0;i!=taskOfLeaderOverdue.size();i++)
			{
				t=taskOfLeaderOverdue.get(i);
				if(t.getDueDate().getEventDate().before(today.getEventDate()))				
				{
						//http://stackoverflow.com/questions/3299972/difference-in-days-between-two-dates-in-java
						todayTimeMilis=today.getEventDate().getTimeInMillis();
						dueTimeMilis=t.getDueDate().getEventDate().getTimeInMillis();
						diff =todayTimeMilis- dueTimeMilis;
						diffDays = diff / (24 * 60 * 60 * 1000);//difference in days
						if(diffDays>=days)
						{
							t.printTask();
						System.out.println(diffDays);
						found=true;
						}
						else
						{
							taskOfLeaderOverdue.remove(t);
							i--;
						}	
				}
				else
				{
					taskOfLeaderOverdue.remove(t);//remove it from arraylist if is not overdue
					i--;
				}
			}
		if(found==false)
		{
			System.out.println("No Task found in that date range");
			return null;
		}
		
		return taskOfLeaderOverdue;
				
	}
	
	public void saveTaskStore()
	{
		System.out.println("Enter file name to save Task store");
		String file= kb.next();
		file=file+".bin";
		this.saveTaskStore(file);
	}
	
	public void saveTaskStore(String filename)
	{
		SerializationUtility.save(filename, this);
	}
	
	
//	public TaskStore loadTaskStore(String filename)
//	{
//		return (TaskStore)SerializationUtility.load(filename);
//	}
	
	public void editTaskById(Employee empStore)
	{
		System.out.println("Please enter task id you want to edit.");
		int id= kb.nextInt();
		this.editTaskById(id,empStore);
	}
	
	public void editTaskById(int id,Employee empStore)
	{
		for(Task t: this.tStore)
		{
			if(t.gettID()==id)
			{
				t.setTask();
				Team myChoosenT = new Team();
				myChoosenT.createTeam(empStore);
				t.setTaskTeam(myChoosenT);
			}
		}
	}
	
	public void compareStores(TaskStore toCompare)
	{
		boolean found = false;
		if(this.tStore.size()<=toCompare.gettStore().size())
		{
			System.out.println("The one you are comparing is bigger or equals.");
			for(Task compare: toCompare.gettStore())
			{
				found=false;
				for(Task t: this.tStore)
				{
					if(compare.equals(t))
					{
						found=true;
						break;
					}
					
				}
				if(found==false)
				{	
					break;
					
				}
				
			}
		}
		else
		{
			System.out.println("The one you are comparing is smaller.");
			for(Task t: this.tStore)
			{
				found=false;
				for(Task compare: toCompare.gettStore())
				{
					if(t.equals(compare))
					{
						found=true;
						break;
					}
					
				}
				if(found==false)
					{
						break;
					}
			}
		}	
		if(found==false)
		{
			System.out.println("Stores are not equals.");
		}
		else
		{
			System.out.println("Stores are equals.");
		}
	}
	
	
	/*public void compareStores(TaskStore toCompare)
	{
		if(this.tStore.size()>=toCompare.gettStore().size())
		{
			if(this.tStore.containsAll(toCompare.gettStore()))
			{
				System.out.println("Stores are equals.");
			}
			else
			{
				System.out.println("Stores are not equals.");
			}
		}
		else
		{
			if(toCompare.gettStore().containsAll(this.tStore))
			{
				System.out.println("Stores are equals.");
			}
			else
			{
				System.out.println("Stores are not equals.");
			}
		}
		
	}*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
