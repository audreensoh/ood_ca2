import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;


public class Team implements Serializable
{
	private static Scanner kb= new Scanner(System.in);
	private Person tLeader;
	private ArrayList<Person> teammates;
	
	public Team()
	{
		this.tLeader = new Person();
		this.teammates = new ArrayList<Person>();
		
	}
	
	public Team(Person tLeader, ArrayList<Person> teammates) {
		//super();
		this.tLeader = tLeader;
		this.teammates = teammates;
	}

	public Team(Person tLeader) {
		//super();
		this.tLeader = tLeader;
		this.teammates = new ArrayList<Person>();
	}
	
	public Person gettLeader() {
		return tLeader;
	}

	public void settLeader(Person tLeader) {
		this.tLeader = tLeader;
	}

	public ArrayList<Person> getTeammates() {
		return teammates;
	}

	public void setTeammates(ArrayList<Person> teammates) {
		this.teammates = teammates;
	}

	@Override
	public String toString() {
		
		//for(Person p: this.teammates)
		//{
			
		//}
		return "\nTeam Leader=\t\t" + tLeader ;
				
	}
	
	public void printTeam()
	{
		tLeader.printPerson();
		for(Person p: this.teammates)
		{
			p.printPerson();
			
		}
	}
	public void addTeammate(Person p1)
	{
		this.teammates.add(p1);
		
	}
	public void createTeam(Employee empStore)
	{
		System.out.println("Please enter leaders id:");
		String id = kb.next();
		this.tLeader = empStore.getAnyEmployee(id);
		System.out.println("Please enter how many teammates do you want in team:");
		int num = kb.nextInt();
		String memberId;
		for(int i=0;i!=num;i++)
		{
			System.out.println("Please enter members" +i+" id:");
			memberId= kb.next();
			this.teammates.add(empStore.getAnyEmployee(memberId));
		}
		
	}
	
}

































