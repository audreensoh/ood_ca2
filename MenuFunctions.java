import java.util.Scanner;


public class MenuFunctions {
	public static Scanner enter = new Scanner(System.in);
	
	public static void printMenu(Employee empStore,TaskStore tskStore)
	{
		System.out.println("Menu: ");
		System.out.println("Press 1 to manage employees");
		System.out.println("      2 to manage tasks ");
		System.out.println("      3 to save employee store");
		System.out.println("      4 to load employee store");
		System.out.println("      5 to save task store");
		System.out.println("      6 to load task store");
		getInputMenu1(empStore,tskStore);
	}
	public static void getInputMenu1(Employee empStore,TaskStore tskStore)
	{
		int input= enter.nextInt();
		if(input==1)
		{
			printEmployeeMenu(empStore,tskStore);
		}else if(input==2)
		{
			printTaskMenu(empStore,tskStore);
		}else if(input==3)
		{
			empStore.saveEmployeeStore();
		}else if(input==4)
		{
			/*System.out.println("Enter file name to load employee store.");
			String file= enter.next();
			file=file+".bin";*/
			empStore = (Employee)SerializationUtility.load("e.bin");
			
		}else if(input==5)
		{
			tskStore.saveTaskStore();
		}else if(input==6)
		{
			/*System.out.println("Enter file name to load task store.");
			String file= enter.next();
			file=file+".bin";*/
			tskStore = (TaskStore)SerializationUtility.load("t.bin");
		}else
		{
			System.out.println("Invalid input.");
		}
		
	}
	public static void printEmployeeMenu(Employee empStore,TaskStore tskStore)
	{
		System.out.println("Employee Menu: ");
		System.out.println("Press 1 to add Employee");
		System.out.println("      2 to delete Employee ");
		System.out.println("      3 to edit Employee ");
		System.out.println("      4 to print all Employees");
		System.out.println("      5 to print details of an Employee ");
		System.out.println("      0 to go back to main menu");
		getInputMenuEmployee(empStore,tskStore);
	}
	
	public static void getInputMenuEmployee(Employee empStore,TaskStore tskStore)
	{
		int input= enter.nextInt();
		if(input==1)
		{
			empStore.addPerson();
		}else if(input==2)
		{
			empStore.deleteEmployee();
		}else if(input==3)
		{
			empStore.editEmployeeById();
		}else if(input==4)
		{
			empStore.printEmployees();
		}else if(input==5)
		{
			empStore.printDetailsOfAnEmplyee();
		}else if(input==0)
		{
			printMenu(empStore,tskStore);
		}else
		{
			System.out.println("Invalid input.");
		}
		
	}
	public static void printTaskMenu(Employee empStore,TaskStore tskStore)
	{
		System.out.println("Task Menu: ");
		System.out.println("Press 1 to add Task");
		System.out.println("      2 to delete Task");
		System.out.println("      3 to edit Task");
		System.out.println("      4 to print all Task of a leader");
		System.out.println("      5 to print all Task of same status");
		System.out.println("      6 to print all Task due within a range");
		System.out.println("      7 to print Task sorted by status");
		System.out.println("      8 to copy Task store");
		System.out.println("      9 to print a list of task overdue in a number of days by certain leader");
		System.out.println("      c to compare two stores for equality");
		System.out.println("      b to go back to main menu");
		getInputMenuTask(empStore,tskStore);
	}
	
	public static void getInputMenuTask(Employee empStore,TaskStore tskStore)
	{
		String input= enter.next();
		if(input.equalsIgnoreCase("1"))
		{
			tskStore.addTask(empStore);
		}else if(input.equalsIgnoreCase("2"))
		{
			tskStore.deleteTask();
		}else if(input.equalsIgnoreCase("3"))
		{
			tskStore.editTaskById(empStore);
		}else if(input.equalsIgnoreCase("4"))
		{
			tskStore.printTaskWithSameLeader();
		}else if(input.equalsIgnoreCase("5"))
		{
			tskStore.getSameStatus();
		}else if(input.equalsIgnoreCase("6"))
		{
			tskStore.getTaskBetweenTwoDatesPrint();
		}else if(input.equalsIgnoreCase("7"))
		{
			tskStore.getSortedByStatusPrint();
		}else if(input.equalsIgnoreCase("8"))
		{
			tskStore.copyStoreWithSave();
		}else if(input.equalsIgnoreCase("9"))
		{
			tskStore.getTaskOverdueForLeader();
		}
		else if(input.equalsIgnoreCase("c"))
		{
			TaskStore toCheck = new TaskStore();
			toCheck = (TaskStore)SerializationUtility.load("t.bin");
			Person p1=new Person("Lukas Ponik","lukas@gmail.com", "0871794751");
			Person p2=new Person("Audreen Soh","Audreen@gmail.com", "0877552062");
			Employee elist= new Employee();
			elist.addPerson(p1);
			elist.addPerson(p2);
			Team t1=new Team(elist.getAnyEmployee("EMP1"));
			t1.addTeammate(elist.getAnyEmployee("EMP2"));
			Task tsk5= new Task("GUIfffffffffffff design for Samsung S200",t1,new Date(30,8,2013),new Date(31,0,2014),Task.Status.CANCELLED,false);
			toCheck.addTask(tsk5);
			//toCheck.deleteTask(1);
			tskStore.compareStores(toCheck);
		}
		else if(input.equalsIgnoreCase("b"))
		{
			printMenu(empStore,tskStore);
		}else
		{
			System.out.println("Invalid input.");
		}
		
	}
	
}
