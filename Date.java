import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.text.SimpleDateFormat;



public class Date implements Comparable,Serializable
{
	private Calendar eventDate;
	private static Scanner kb= new Scanner(System.in);
	public Date()
	{
		this.eventDate= Calendar.getInstance();
		
	}
	public Date(int date,int month,int year)
	{
		this.eventDate=Calendar.getInstance();
		//for a start all events start at 9am
		this.eventDate.set(year,month,date,9,0,0);
	}

	public Calendar getEventDate() {
		return eventDate;
	}
	
	public void setEventDate() 
	{
		System.out.println("Please enter Date Year in number:");
		int year = kb.nextInt();
		System.out.println("Please enter Date Month in number:");
		int month = kb.nextInt();
		System.out.println("Please enter Date Day in number:");
		int date = kb.nextInt();
		this.eventDate.set(year,month,date,9,0,0);
	}
	
	public void setEventDate(Calendar eventDate) {
		this.eventDate = eventDate;
	}
//	public enum Format
//	{
//		TIME_AM_PM,DATE,DATE_AT_TIME; //0,1,2
//		
//		public String toString()
//		{
//			if (this.ordinal()==0)
//				return "hh:mm a";
//			else if (this.ordinal()==1)
//				return "dd MMM yyyy";
//			else
//				return "dd/MM/yy 'at' hh:mm:ss";
//		}
//	}
//	
//	
//	public static String format(Calendar c, Format f)
//	{
//		
//		SimpleDateFormat dateFormat=new SimpleDateFormat(f.toString());
//		
//		String strDate=dateFormat.format(c.getTime());
//
//		return strDate;
//		
//		//faster & shorter way
//		//return new SimpleDateFormat(f).format(c.getTime());
//	}
	@Override
	public String toString() {
		SimpleDateFormat dateFormat=new SimpleDateFormat("dd MMM yyyy");
		String strDate=dateFormat.format(this.eventDate.getTime());
		return strDate;
	}

	@Override
	public int compareTo(Object obj) {
		if(obj == null)
			return 0;
		if(this.getClass() != obj.getClass())
		return 0;
		
		Date other=(Date)obj;
		
		if(this.eventDate.before(other.getEventDate()))
			return -1;
		else if(this.eventDate.after(other.getEventDate()))
			return 1;
		else
			return 0;
	}
	
	
	public void printDate()
	{
		System.out.println(this.toString());		
	}
}
