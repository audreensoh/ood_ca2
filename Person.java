import java.io.Serializable;
import java.util.Scanner;


public class Person implements Serializable
{
	private String pName;
	private String pId;
	private String pemail;
	private String ptel;
	private static Scanner kb= new Scanner(System.in);
	
	public Person()
	{
		this.pName = "No name";
		this.pId = "No ID";
		this.pemail = "No email";
		this.ptel = "No tel";
	}
	public Person(String pName, String pemail, String ptel) {
		super();
		this.pName = pName;
		this.pId = "No ID";
		this.pemail = pemail;
		this.ptel = ptel;
	}
	
	
	public Person(String pName, String pId, String pemail, String ptel) {
		super();
		this.pName = pName;
		this.pId = pId;
		this.pemail = pemail;
		this.ptel = ptel;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public String getPemail() {
		return pemail;
	}
	public void setPemail(String pemail) {
		this.pemail = pemail;
	}
	public String getPtel() {
		return ptel;
	}
	public void setPtel(String ptel) {
		this.ptel = ptel;
	}
	@Override
	public String toString() {
		return "\nName=\t\t" + pName +
				"\nId=\t\t" + pId 
				+ "\nEmail=\t\t" + pemail
				+ "\nTel=\t\t" + ptel ;
	}
	
	public void printPerson()
	{
		System.out.println(this.toString());
	}
	
	public void editPerson()
	{
		
		System.out.println("Enter new Name:");
		String name= kb.next();
		this.pName=name;
		System.out.println("Enter new Email:");
		String email= kb.next();
		this.pemail=email;
		System.out.println("Enter new Tel:");
		String ptel= kb.next();
		this.ptel=ptel;
		
	}
	
	
	
}
