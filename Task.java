import java.io.Serializable;
import java.util.Calendar;
import java.util.Scanner;

public class Task implements Serializable
{
	
	private static Scanner kb= new Scanner(System.in);
	private String tName;
	private int tID;
	private Team taskTeam;
	private Date addDate;
	private Date dueDate;
	private Date completeDate;
	private Status status;
	//class variable of the shared string formats
	private static String[] statusNames= {"Cancelled","Completed",
			"Ongoing","Paused"};
		
	//1 = true its over due & 0 = false not overdue
	private boolean overdue;
	
	public enum Status
	{
		CANCELLED,COMPLETED,ONGOING,PAUSED;
		
		public String toString()
		{	
			return statusNames[this.ordinal()];	
		}
	}
	
	public Task()
	{
		this.tName = "No task name";
		this.tID = -99;
		this.taskTeam = null;
		this.addDate = new Date();
		this.dueDate = new Date();
		this.completeDate = new Date();
		this.status = Status.CANCELLED;
		this.overdue = false;
		
	}
	//full constructor no id
	public Task(String tName, Team taskTeam,Date addDate,Date dueDate,Date completeDate,Status s,
			boolean overdue) {
		//super();
		this.tName = tName;
		this.taskTeam = taskTeam;
		this.addDate = addDate;
		this.dueDate = dueDate;
		this.completeDate = completeDate;
		this.status = s;
		this.overdue = overdue;
	}
	//no task id in this one no complete date in this one
	public Task(String tName, Team taskTeam,Date addDate,Date dueDate,Status s,
			boolean overdue) {
		//super();
		this.tName = tName;
		this.taskTeam = taskTeam;
		this.addDate = addDate;
		this.dueDate = dueDate;
		this.status = s;
		this.overdue = overdue;
	}
	
	public String gettName() {
		return tName;
	}
	public void settName(String tName) {
		this.tName = tName;
	}
	public int gettID() {
		return tID;
	}
	public void settID(int tID) {
		this.tID = tID;
	}
	public Team getTaskTeam() {
		return taskTeam;
	}
	public void setTaskTeam(Team taskTeam) {
		this.taskTeam = taskTeam;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Date getCompleteDate() {
		return completeDate;
	}
	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public boolean isOverdue() {
		return overdue;
	}
	public void setOverdue(boolean overdue) {
		this.overdue = overdue;
	}
	@Override
	public String toString() {
		return "\nTask=\t\t" + tName +
				"\ntID=\t\t" + tID + 
				"taskTeam="	+ taskTeam + 
				"addDate=" + addDate + 
				"dueDate=" + dueDate+ 
				"completeDate=" + completeDate + 
				"status=" + status
				+ "overdue=" + overdue ;
	}
	
	public void printTask()
	{
		System.out.println("\nTask=\t\t" + tName);
		System.out.println("ID=\t\t" + tID);
		this.taskTeam.printTeam();
		System.out.println("Added Date=\t"+this.addDate.toString());
		System.out.println("Due Date=\t"+this.dueDate.toString());
		if(this.status == this.status.COMPLETED )
		{
			System.out.println("Completed Date=\t"+this.completeDate.toString());
		}
		else
		{
			System.out.println("Completed Date=\t"+"Not yet completed");
		}
		System.out.println("Status=\t\t"+this.status.toString());
		if(this.overdue==true)
		System.out.println("Notify=\t\t"+"It is overdue");
		else
			System.out.println("Notify=\t\t"+"It is not overdue");
			
			
		
		
	}
	
	public void addTask()
	{
		
		
	}
	public void setTask()
	{
		System.out.println("Please enter Description of the task:");
		String desc = kb.nextLine();
		this.tName = desc;
		this.addDate= new Date();
		System.out.println("Please set the due date:");
		this.dueDate = new Date();
		dueDate.setEventDate();
		this.completeDate = new Date();
		this.chooseStatus();		
	}

	public void chooseStatus()
	{
		System.out.println("Please choose Status:");
		System.out.println("Enter  1 for CANCELLED");
		System.out.println("Enter  2 for ONGOING");
		System.out.println("Enter  3 for PAUSED");
		System.out.println("Enter  4 for COMPLETED");
		Scanner bb= new Scanner(System.in);
		int input = bb.nextInt();
		
		if(input==1)
		{
			this.setStatus(Status.CANCELLED);
		}else if(input==2)
		{
			this.setStatus(Status.ONGOING);
		}else if(input==3)
		{
			this.setStatus(Status.PAUSED);
		}else if(input==4)
		{
			this.setStatus(Status.COMPLETED);
		}
		else
		{
			System.out.println("Invalid input.");
		}	
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)//this mean that the variable stores the same address
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
			
		if(this.tName.equalsIgnoreCase(other.gettName()))
			return true;
		
		return false;
	
	}


}
