
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class SerializationUtility 
{
	public static void save(String name,Object obj)
	{
		try
		{
			//handle to a file output stream
			FileOutputStream fos = new FileOutputStream(name);	
			//handle to an object output stream
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			//write the object to the stream
			oos.writeObject(obj);
			oos.close();
			System.out.println("Successfully saved");

		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Object load(String name)
	{
		Object obj = null;
		
		try
		{
			FileInputStream fis = new FileInputStream(name);
			ObjectInputStream ois = new ObjectInputStream(fis);
			obj = ois.readObject();
			ois.close();
			System.out.println("Successfully loaded");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return obj;
	}
}
